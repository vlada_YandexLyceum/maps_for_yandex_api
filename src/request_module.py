import sys
import requests



def get_image_to_file(coords, scale, type='map', point=None):
    response = None
    try:
        map_request = "https://static-maps.yandex.ru/1.x/?ll={}%2C{}&l={}&spn={}".format(str(coords[0]), str(coords[1]),
                                                                                         type,
                                                                                         str(scale[0]) + ',' + str(
                                                                                             scale[1]))
        if point:
            map_request += "&pt=%s,%s,pm2dbm" % (str(point[0]), str(point[1]))
        response = requests.get(map_request)

        if not response:
            print("Ошибка выполнения запроса:")
            print(map_request)
            print("Http статус:", response.status_code, "(", response.reason, ")")
            sys.exit(1)

    except BaseException as exep:
        print("Запрос не удалось выполнить. Проверьте наличие сети Интернет.")
        print('Error:', exep)
        sys.exit(1)

    # Запишем полученное изображение в файл
    map_file = "map.png"
    try:
        with open(map_file, "wb") as file:
            file.write(response.content)
            return response.content
    except IOError as ex:
        print("Ошибка записи временного файла:", ex)
        sys.exit(2)


def finder_by_name(query):
    toponym_to_find = " ".join(query)

    geocoder_api_server = "http://geocode-maps.yandex.ru/1.x/"

    geocoder_params = {"geocode": query, "format": "json"}

    response = requests.get(geocoder_api_server, params=geocoder_params)

    if not response:
        return [37.646866, 55.725047]
        pass

    json_response = response.json()
    try:
        toponym = json_response["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]
        toponym_coodrinates = toponym["Point"]["pos"]
        toponym_longitude, toponym_lattitude = toponym_coodrinates.split(" ")
        return [float(toponym_longitude), float(toponym_lattitude)]
    except:
        return [-87.04897, 14.78444]


def get_address(object):
    try:
        geocoder_request = "http://geocode-maps.yandex.ru/1.x/?geocode=%s&format=json" % object
        # Выполняем запрос.
        response = None

        response = requests.get(geocoder_request)
        if response:
            # Преобразуем ответ в json-объект
            json_response = response.json()
            # print(json_response)
            # Получаем первый топоним из ответа геокодера.
            # Согласно описанию ответа, он находится по следующему пути:
            toponym = json_response["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]
            # Полный адрес топонима:
            toponym_address = toponym["metaDataProperty"]["GeocoderMetaData"]["text"]
            # Координаты центра топонима:
            toponym_coodrinates = toponym["Point"]["pos"]
            # Печатаем извлечённые из ответа поля:
            return toponym_address
        else:
            print("Ошибка выполнения запроса:")
            print(geocoder_request)
            print("Http статус:", response.status_code, "(", response.reason, ")")
            return ''
    except BaseException:
        print('Error!')
        return ''


def get_postal_code(object):
    try:
        geocoder_request = "http://geocode-maps.yandex.ru/1.x/?apikey=e4cd438d-0126-4cdf-b885-f62200431149&&geocode={}&format=json".format(object)
        response = requests.get(geocoder_request)
        if response:
            json_response = response.json()
            toponym = json_response["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]
            toponym_p = toponym["metaDataProperty"]["GeocoderMetaData"]["Address"]["postal_code"]
            return str(toponym_p)
        else:
            print("Ошибка выполнения запроса:")
            print(geocoder_request)
            print("Http статус:", response.status_code, "(", response.reason, ")")
    except BaseException:
        return 'Этот объект не имеет индекса'

