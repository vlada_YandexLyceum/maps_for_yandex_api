import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from UI_module import MyWidget


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MyWidget()
    ex.show()
    sys.exit(app.exec())
