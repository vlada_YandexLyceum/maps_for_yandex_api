from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import requests
from request_module import  finder_by_name, get_address
from request_module import get_image_to_file, get_postal_code

class MyWidget(QMainWindow):
    def __init__(self):
        super().__init__()
        self.spn = 0.01
        self.coords= [37.620070, 55.753630]
        self.coords_pt = self.coords
        self.coords = [37.620070, 55.753630]
        self.type = 'map'
        self.point_coords = None
        self.postal_code_true = False
        self.initUI()
        self.updateImage()

    def initUI(self):
        self.setWindowTitle('Map')
        self.label = QLabel(self)

        map_button = QRadioButton('map', self)
        map_button.move(10, 10)
        map_button.clicked.connect(self.set_type)

        sat_button = QRadioButton('sat', self)
        sat_button.move(10, 60)
        sat_button.clicked.connect(self.set_type)

        combi_button = QRadioButton('sat,skl', self)
        combi_button.move(10, 110)
        combi_button.clicked.connect(self.set_type)

        self.search_button = QPushButton('Искать', self)
        self.search_button.resize(self.search_button.sizeHint())
        self.search_button.move(30, 470)
        self.search_button.clicked.connect(self.find_object_by_name)

        self.search_query = QLineEdit(self)
        self.search_query.resize(115, 22)
        self.search_query.move(120, 470)

        delete_button = QPushButton('Delete', self)
        delete_button.move(250, 470)
        delete_button.resize(delete_button.sizeHint())
        delete_button.clicked.connect(self.delete_point)

        self.address_output = QLabel(self)
        self.address_output.resize(350, 22)
        self.address_output.move(350, 470)

        self.check_postal_code = QCheckBox('Показать почтовый индекс', self)
        self.check_postal_code.resize(self.check_postal_code.sizeHint())
        self.check_postal_code.move(30, 510)
        self.check_postal_code.stateChanged.connect(self.define_postal_code)

        self.postal_code = QLabel(self)
        self.postal_code.resize(200, 22)
        self.postal_code.move(200, 505)


    def define_postal_code(self, state):
        if state == Qt.Checked:
            self.postal_code.setText(get_postal_code(self.search_query.text()))
            self.postal_code_true = True
        else:
            self.postal_code.setText('')
            self.postal_code_true = False


    def set_type(self, pressed):
        source = self.sender()
        self.type = source.text()
        self.updateImage()

    def updateImage(self):
        bytes = get_image_to_file(self.coords, (self.spn, self.spn), type=self.type, point=self.point_coords)
        pixmap = QPixmap('map.png', format='PNG')
        img = QPixmap()
        img.loadFromData(bytes)

        self.label.setPixmap(img)
        self.label.resize(600, 450)

        self.resize(600, 550)

    def find_object_by_name(self):
        query = self.search_query.text()
        self.coords = finder_by_name(query)
        self.point_coords = self.coords.copy()
        self.address_output.setText(get_address(query))
        if self.postal_code_true:
            self.postal_code.setText(get_postal_code(self.search_query.text()))
        else:
            self.postal_code.setText('')
        self.updateImage()
        self.setFocus()

    def delete_point(self):
        self.postal_code.setText('')
        self.search_query.setText('')
        self.address_output.setText('')
        self.point_coords = None
        self.updateImage()

    def keyPressEvent(self, event):
        map_step_1 = self.spn
        map_step_0 = self.spn * 2
        if event.key() == Qt.Key_PageUp:
            if self.spn * 0.5 > 0.0003:
                self.spn *= 0.5
        elif event.key() == Qt.Key_PageDown:
            if self.spn * 2 < 90:
                self.spn *= 2
        elif event.key() == Qt.Key_Down:
            if self.coords[1] - map_step_1 > 85 or self.coords[1] - map_step_1 < -85:
                self.coords[1] *= -1
            self.coords[1] -= map_step_1
        elif event.key() == Qt.Key_Up:
            if self.coords[1] + map_step_1 > 85 or self.coords[1] + map_step_1 < -85:
                self.coords[1] *= -1
            self.coords[1] += map_step_1
        elif event.key() == Qt.Key_Left:
            if self.coords[0] + map_step_0 > 180 or self.coords[0] + map_step_0 < -180:
                self.coords[0] *= -1
            self.coords[0] -= map_step_0
        elif event.key() == Qt.Key_Right:
            if self.coords[0] - map_step_0 > 180 or self.coords[0] - map_step_0 < -180:
                self.coords[0] *= -1
            self.coords[0] += map_step_0
        self.updateImage()
